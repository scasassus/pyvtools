This is a python 3 package to view fits files.

First update view.py to your settings and make it executable. Place the modules Vtools.py and Cube2Im.py in your parth (i.e. with sys.path.append in view.py).

Then try it with the provide FITS file example, 

view.py V4046_ALMA_zoom.fits

for a contour plot overlay:

view.py V4046_ALMA_zoom.fits V4046_ALMA_zoom.fits


Strike h for help in the gui window


EXTERNAL DEPENDENCIES: astropy